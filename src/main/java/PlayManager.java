import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Element;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ksheppard on 13/04/2016.
 */
public class PlayManager {
    //private static Map<String, String> systemProperties = ManagementFactory.getRuntimeMXBean().getSystemProperties();
    protected static String getWorkingDirectory() {
        String _path;
        if(System.getProperty("cwd") != null) _path = System.getProperty("cwd");
        else _path = System.getProperty("user.dir");
        return Util.parsePath(_path);
    }
    protected static String getHomeDirectory() {
        return Util.parsePath(System.getProperty("playmgr.home"));
    }

    public static void main(String[] args) {
        Options options = new Options();
        options.addOption("init", false, "Creates a settings.xml file in the current directory.");
        options.addOption("start", false, "Starts the application in the current directory.");
        options.addOption("stop", false, "Stops the application in the current directory.");
        options.addOption("serve", false, "Starts all the applications defined in apps.xml");
        options.addOption("help", false, "Lists all commands");

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);
            if(cmd.hasOption("help")) {
                System.out.println("Below are the commands for the playmgr tool. Use: playmgr -<command>");
                for(Option o : options.getOptions()) {
                    System.out.format("-%-20s%s%n", o.getOpt(), o.getDescription());
                }
            }
            if(cmd.hasOption("init")) {
                System.out.print("Creating settings file...");
                //Copy settings.xml to current working directory and exit
                FileUtils.copyFile(new File(getHomeDirectory() + "settings.xml"), new File(getWorkingDirectory() + "settings.xml"));
                System.out.println("Done!");
                System.out.println("Modify settings.xml to customize app launch parameters.");
            }
            if(cmd.hasOption("serve")) {
                XMLFile apps = new XMLFile(new File(getHomeDirectory() + "apps.xml"));
                for(Element e : apps.getElementsByTagName("application")) {
                    String workingDirectory = Util.parsePath(e.getAttribute("path"));
                    new Application(workingDirectory).start();
                }
            }
            if(cmd.hasOption("start")) {
                //Read settings.xml from the current directory and run Application.start()
                new Application(getWorkingDirectory()).start();
            }
            if(cmd.hasOption("stop")) {
                //Read settings.xml from the current directory and run Application.stop()
                new Application(getWorkingDirectory()).stop();
            }
        } catch (ParseException | IOException e) {
            showErrorAndExit(e, 500);
        }

    }

    protected static void log(String message) {
        try {
            DateFormat format = new SimpleDateFormat("MMM d, yyyy hh:mm:ss a");
            FileWriter writer = new FileWriter(getHomeDirectory() + "log.txt", true);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            PrintWriter out = new PrintWriter(bufferedWriter);
            out.println(format.format(new Date())+": " + message);
            out.close();
        } catch (IOException e) {
            showErrorAndExit(e, 404);
        }
    }

    protected static void showErrorAndExit(Exception e, int code) {
        System.err.println(e.getMessage());
        System.exit(code);
    }
}

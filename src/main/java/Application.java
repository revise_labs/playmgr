import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by ksheppard on 13/04/2016.
 */
public class Application {
    private XMLFile document;
    private List<String> commandArgs;
    private String cwd;

    public Application(String directory) {
        cwd = directory;
    }

    private File getSettingsFile() {
        return new File(cwd + "settings.xml");
    }

    public void start() {
        cleanUp();
        commandArgs = new ArrayList<>();
        if(!getSettingsFile().exists()) {
            System.out.println("Cannot find file settings.xml. Run 'playmgr init' to create it.");
            PlayManager.log("App in folder " + cwd + " is missing a settings.xml file.");
        }
        try {
            document = new XMLFile(getSettingsFile());
            commandArgs.add(playExecutable());
            setJVMOptions();
            commandArgs.add("start");
            ProcessBuilder builder = new ProcessBuilder(commandArgs);
            builder.directory(new File(cwd));
            Process playApp = builder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(playApp.getInputStream()));
            String s;
            while((s = reader.readLine()) != null) {
                System.out.println(s);
                if(s.matches("(.*)Listening for HTTP(.*)")) {
                    System.out.println("\nApplication launched! Exiting logs.\n");
                    PlayManager.log("Application in folder " + cwd + " started successfully.");
                    break;
                }
                if(s.matches("Oops(.*)")) {
                    System.out.println("\n Application not started.");
                    PlayManager.log("Application not started: " + s);
                    break;
                }
            }
            reader.close();
        } catch (IOException e) {
            PlayManager.log(e.getMessage());
        }
    }

    public void stop() {
        File PIDFile = new File(cwd + "RUNNING_PID");
        System.out.print("Terminating application...");
        if(PIDFile.exists()) {
            try {
                Scanner scanner = new Scanner(new FileInputStream(PIDFile));
                String PID = scanner.nextLine();
                scanner.close();
                Process taskKill = Runtime.getRuntime().exec(new String[] {"taskkill", "/F", "/PID", PID});
                taskKill.waitFor();
                System.out.println("Application terminated.");
                System.out.print("Deleting PID File... ");
                if(PIDFile.delete()) {
                    System.out.println("PID file deleted!");
                } else {
                    System.err.println("Delete operation failed.");
                }
            } catch (IOException | InterruptedException e) {
                PlayManager.log(e.getMessage());
            }
        }
        else {
            System.out.println("This application isn't running :-/");
        }
    }

    private void cleanUp() {
        File PIDFile = new File(cwd + "RUNNING_PID");
        if(PIDFile.exists()) {
            System.out.print("Application is possibly running. ");
            stop();
        }
    }

    private void setJVMOptions() {
        Element optionsElement = document.getElementsByTagName("jvmOptions").get(0);
        if(optionsElement != null) {
            NodeList optionsList = optionsElement.getElementsByTagName("option");
            for(Element e : XMLFile.pluckElements(optionsList)) {
                commandArgs.add("-D"+e.getAttribute("name") + "=" + e.getAttribute("value"));
            }
        }
    }

    private String playExecutable() {
        List<Element> elements = document.getElementsByTagName("play");
        String executable = "play.bat", playHome = System.getenv().get("PLAY_HOME");;
        if(!elements.isEmpty()) executable = Util.parsePath(elements.get(0).getAttribute("path")) + "play.bat";
        else if(playHome != null) executable = Util.parsePath(playHome) + "play.bat";
        return executable;
    }

    private String[] commandArgsToArray() {
        String[] args = new String[commandArgs.size()];
        args = commandArgs.toArray(args);
        return args;
    }
}

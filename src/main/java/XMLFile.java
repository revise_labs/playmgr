import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ksheppard on 14/04/2016.
 */
public class XMLFile {
    private Document document;
    public XMLFile(File file) {
        try {
            document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file);
        } catch (SAXException | ParserConfigurationException | IOException e) {
            PlayManager.log(e.getMessage());
            e.printStackTrace();
        }
    }

    public List<Element> getElementsByTagName(String tagName) {
        NodeList nodeList = document.getElementsByTagName(tagName);
        return pluckElements(nodeList);
    }

    public static List<Element> pluckElements(NodeList nodeList) {
        List<Element> elements = new ArrayList<>();
        for(int i = 0; i< nodeList.getLength(); i++) {
            if(nodeList.item(i).getNodeType() == Element.ELEMENT_NODE) {
                elements.add((Element) nodeList.item(i));
            }
        }
        return elements;
    }


}

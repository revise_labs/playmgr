import java.io.File;

/**
 * Created by ksheppard on 14/04/2016.
 */
public class Util {
    public static String parsePath(String path) {
        if(path == null) return null;
        return new File(path).getAbsolutePath().replace("\\", "/") + "/";
    }
}
